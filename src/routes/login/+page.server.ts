import { checkUserCredential, createUser } from '$lib/database';
import { createSession } from '$lib/sessionStore';
import { fail, redirect, type Cookies } from '@sveltejs/kit';
import type { Actions } from './$types';

/**
 * Se charge de créer une session côté serveur et un cookie est déposé chez le client
 * @param cookie récupère le type `Cookies` fourni par SK
 * @param username est le nom d'utilisateur transmis par le formulaire
 */
const performLogin = (cookie: Cookies, username: string) => {
    const maxAge = 60 * 60;
    const sid = createSession(username, maxAge)
    cookie.set("sid", sid, {
        maxAge
    });
}

export const actions = {
    login: async ({ request, cookies }) => {
        const req = await request.formData()
        const username = req.get("username")?.toString();
        const password = req.get("password")?.toString();

        if (username && password) {
            const rslt = await checkUserCredential(username, password)
            if (!rslt) {
                console.log("Appel de checkUserCredential : échec");
                return fail(400, { errorMessage: "Incorrect username or password" })
            }
            performLogin(cookies, username);
            throw redirect(303, "/");
        } else {
            return fail(400, { errorMessage: "Missing username or password" })
        }
    },
    register: async ({ request, cookies }) => {
        const req = await request.formData()
        const username = req.get("username")?.toString();
        const password = req.get("password")?.toString();

        if (username && password) {
            await createUser(username, password);
            console.log(`Register OK avec : ${username}, ${password}`);
            performLogin(cookies, username);
            throw redirect(303, "/")
        }
        else return fail(400, { errorMessage: "Missing username or password" })

    }
} satisfies Actions;