import type { LayoutServerLoad } from "./$types";

export const load = (async ({ locals }) => {
    const username = locals.username;
    return { username }
}) satisfies LayoutServerLoad;