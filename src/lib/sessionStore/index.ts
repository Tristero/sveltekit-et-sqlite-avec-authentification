import { getUserRoles } from '$lib/database';
import { randomBytes } from 'node:crypto';

type SessionInfo = {
    username: string,
    roles: string[],
    invalidAt: number,
};

type SID = string;

const getSID = (): SID => {
    return randomBytes(32).toString("hex");
}

const sessionStore = new Map<SID, SessionInfo>();

let nextClean = Date.now() + 1000 * 60 * 60; // calcul : à partir de ce moment + 1 heure

const clean = () => {
    const now = Date.now();
    for (const [sid, session] of sessionStore) {
        // si la session a dépassé sa date de validité alors on supprime la clé et sa valeur 
        // dans notre sessionStore
        if (session.invalidAt < now) {
            sessionStore.delete(sid);
        }
    }
    nextClean = Date.now() + 1000 * 60 * 60; // on réajuste : une heure de session supplémentaire
}

export const createSession = (username: string, maxAge: number): string => {
    let sid: SID;

    // création de l'ID de session
    do {
        sid = getSID();
    } while (sessionStore.has(sid));

    // récupération des rôles
    const roles = getUserRoles(username);

    const sInfo: SessionInfo = {
        username,
        roles,
        invalidAt: maxAge + (Date.now() * 1000 * 60 * 60), // ici le temps est en milliseconde
    };

    // sauvegarde dans la map
    sessionStore.set(sid, sInfo);

    // on nettoie le sessionStore si besoin
    // indépendamment du retour
    if (Date.now() > nextClean) {
        setTimeout(() => { clean() }, 5000);
    }
    return sid;
}

export const getSession = (sid: SID): SessionInfo | undefined => {
    const sinfo = sessionStore.get(sid);
    if (sinfo) {
        if (Date.now() > sinfo.invalidAt) {
            sessionStore.delete(sid);
            return undefined;
        }
        return sinfo;
    }
    return undefined;
}

export const deleteSession = (sid: SID) => {
    sessionStore.delete(sid)
};