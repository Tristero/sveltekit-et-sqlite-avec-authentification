import Database from "better-sqlite3";
import { DB_PATH } from '$env/static/private';
import type { Album, AlbumTracks, Track } from "./types";
import { hash, compare } from "bcrypt";

const db = Database(DB_PATH);

export const getInitialTracks = (limit: number): Track[] => {

    const sql = `select 
    t.TrackId as trackId, 
    t.Name as trackName, 
    a.AlbumId as albumId, 
    a.Title as albumTitle, 
    at.ArtistId as artistId, 
    at.Name as artistName, 
    g.Name as genre
from tracks t 
    join albums a 
        on t.AlbumId = a.AlbumId 
    join artists at 
        on a.ArtistId = at.ArtistId 
    join genres 
        g on t.GenreId = g.GenreId
limit ${limit}
`;
    const rslt = db.prepare(sql).all();
    return rslt as Track[];
};

export const getAlbumById = (id: number) => {
    const sql = `select 
   a.AlbumId as albumId,
   a.Title as albumTitle,
   at.ArtistId as artistId,
   at.Name as artistName
from albums a
join artists at on a.AlbumId = at.ArtistId
where a.AlbumId =$id`;

    const rslt = db.prepare(sql).get({ id })
    return rslt as Album;
};

export const getAlbumTracks = (id: number) => {
    const sql = `select 
 t.TrackId as trackId,
 t.Name as trackName,
 t.Milliseconds as trackMs
from tracks t
where t.AlbumId = $id
order by t.TrackId`;
    const rslt = db.prepare(sql).all({ id });
    return rslt as AlbumTracks[];
}

export const updateAlbumTitle = (albumId: number, newAlbumTitle: string) => {
    const sql = `update albums
    set Title = $newAlbumTitle
    where AlbumId = $albumId`;
    db.prepare(sql).run({ newAlbumTitle, albumId })
    //const rslt = db.prepare(sql).run({ newAlbumTitle, albumId })
    //console.log(`Changement = ${rslt.changes}. RowId = ${rslt.lastInsertRowid}`);
}

export const searchTracks = (searchTerm: string, limit: number): Track[] => {

    const sql = `select 
    t.TrackId as trackId, 
    t.Name as trackName, 
    a.AlbumId as albumId, 
    a.Title as albumTitle, 
    at.ArtistId as artistId, 
    at.Name as artistName, 
    g.Name as genre
from tracks t 
    join albums a 
        on t.AlbumId = a.AlbumId 
    join artists at 
        on a.ArtistId = at.ArtistId 
    join genres 
        g on t.GenreId = g.GenreId
where lower(t.Name) like lower('%' || $searchTerm || '%')
limit $limit
`;
    const rslt = db.prepare(sql).all({ searchTerm, limit });

    return rslt as Track[];
};

export const createUser = async (username: string, password: string) => {
    const sql = `INSERT INTO users (username, password, roles)
values ($username, $password, 'admin:moderator')`;
    const hashedPWD = await hash(password, 12)

    db.prepare(sql).run({ username, password: hashedPWD });
}

/**
 *  Vérifie si le nom d'utilisateur et son mot de passe existe dans la base de données
 *  @params username : le nom d'utilisateur
 *  @params password : le mot de passe
 *  @returns une promesse avec un booléen
 */
export const checkUserCredential = async (username: string, password: string): Promise<boolean> => {
    const sql = `SELECT password FROM users WHERE username = $username`;
    const rec = db.prepare(sql).get({ username });
    if (rec) {
        return compare(password, rec.password);
    }
    await hash(password, 12)
    return false;
}

/**
 *  récupère les roles pour un utilisateur à partir de son nom d'utilisateur
 *  @params username : le nom d'utilisateur
 *  @returns un tableau de roles (strings)
 */
export const getUserRoles = (username: string): string[] => {

    const sql = `SELECT roles FROM users WHERE username = $username`;
    const rslt = db.prepare(sql).get({ username });
    if (rslt) {
        return rslt.roles.split(":");
    }
    return [];
}