import { getSession } from '$lib/sessionStore';
import type { Handle } from '@sveltejs/kit';

export const handle = (async ({ event, resolve }) => {
    const cookie = event.cookies;
    const sid = cookie.get("sid");

    if (sid) {
        const session = getSession(sid);
        if (session) {
            event.locals.username = session.username;
            event.locals.roles = session.roles;
        } else {
            cookie.delete("sid");
        }
    }
    return await resolve(event);
}) satisfies Handle;