# Authentification et autorisation avec SK et SQLite 3

Notes d'après le tutoriel de [P. Hartenfeller](https://www.youtube.com/watch?v=XRa-b5E7x8w)

Le tutoriel repose sur celui consacré à SK et SQLite3 : le code est repris tel quel. Nous utilisons les bibliothèques suivantes :

- [better-sqlite3](https://www.npmjs.com/package/better-sqlite3) et les types associés (`npm i --save-dev @types/better-sqlite3`)
- [Bulma](https://bulma.io/) en version CDN pour les styles

Nous aurons besoin de la base de données chinook.db disponible sur [sqlitetutorial.net](https://www.sqlitetutorial.net/sqlite-sample-database/).

Enfin une connaissance intermédiaire de SK est nécessaire.

Nous allons voir :

- comment gérer l'enregistrement, l'authentification et les autorisations avec SK
- comment créer des sessions temporaires
- comment gérer les cookies
- comment faire des redirections
- comprendre les hooks et comment les utiliser

Le sujet est assez complexe. Pour éviter toute lourdeur supplémentaire nous allons utiliser en local SQLite et pour nos sessions une Map JS/TS. Tout ceci n'est pas à utiliser lors d'un développement d'une application en mode production bien évidemment.

Dans un premier temps, nous allons améliorer l'interface et créer un formulaire avec deux boutons : l'un pour enregistrer un nouvel utilisateur et un autre pour se connecter.

## 1-Amélioration de l'interface UI

Nous allons rajouter un bandeau accessible sur toutes les pages requêtées par l'utilisateur. Dans `./src/routes/`, nous créons un fichier `+layout.svelte` :

```html
<header>
    <nav class="navbar is-primary px-4" aria-label="main navigation">
        <div class="navbar-brand pr-4">
            <span class="is-size-3 has-text-weight-semibold">SvelteKit + SQLite</span>
        </div>
        <div class="navbar-menu">
            <div class="navbar-start">
                <a href="/" class="navbar-item">Home</a>
            </div>
        </div>

        <div class="navbar-end">
            <div class="navbar-item">
                <div class="buttons">
                    <a href="/login" class="button is-primary">Log in</a>
                </div>
            </div>
        </div>
    </nav>
</header>
<slot />
```

Maintenant nous pouvons créer une nouvelle route `./src/routes/login` avec sa page `+page.svelte` :

```html
<script lang="ts">
</script>

<div class="container">
    <h1 class="is-size-3 has-text-weight-semibold my-4">Login or Register</h1>
    <form method="post">
        <input type="text" class="input my-2" placeholder="Username" name="username" required />
        <input type="password" class="input my-2" placeholder="Password" name="password" required />
        <button type="submit" class="button mt-4 mr-3" formaction="?/register">Register</button>
        <button type="submit" class="button mt-4 is-primary" formaction="?/login">Login</button>
    </form>
</div>
```

**ATTENTION** à ne pas laisser la balise `<FORM>` avec un attribut `action=""` !!!

## 2-Gestion des utilisateurs

Nous allons créer une nouvelle table dans notre fichier `chinook.db`. Cette table va concerner la gestion des utilisateurs. Donc dans sqlite3 :

```sql
CREATE TABLE users (
    username TEXT NOT NULL,
    password TEXT NOT NULL,
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    roles TEXT NOT NULL,
    PRIMARY KEY (username)
)
```

Maintenant nous allons modifier notre fichier `./src/lib/database.ts` pour rajouter des fonctions de gestions des utilisateurs. Nous avons deux boutons un pour le login et un pour enregistrer un nouvel utilisateur.

### 1-Création d'un nouvel utilisateur

Créons une fonction `createUser` qui va récupérer deux paramètres string : le nom d'utilisateur et le mot de passe. On se crée une requête SQL à laquelle on passera 3 valeurs :

- le nom d'utilisateur
- le mot de passe
- une chaîne par défaut "admin:moderator"

Côté `better-sqlite3`, nous utiliserons la fonction `run()` à laquelle nous passerons les deux arguments de la fonction :

```ts
export const createUser = (username: string, password: string) => {
    const sql = `INSERT INTO users (username, password, roles)
values ($username, $password, 'admin:moderator')`;
    db.prepare(sql).run({ username, password });
}
```

**NB** : nous avons écrit `'admin:moderator'` et non `"admin:moderator"` car Sqlite risque de ne pas apprécier l'utilisation des guillemets : il va considérer la seconde écriture comme une colonne ! Donc attention à l'utilisation de guillemets et des apostrophes !!!

### 2-Gestion de l'enregistrement d'un nouvel utilisateur

Comme nous voulons gérer une action particulière ou **nommée**, il convient de créer une `+page.server.ts` (cf.[doc](https://kit.svelte.dev/docs/routing#page-page-server-js))

Créons un squelette de code pour nos deux actions nommées :

```ts
import type { Actions } from './$types';

export const actions = {
    login: async () => {
        console.log("Login")
    },
    register: async () => {
        console.log("Register");
    }
} satisfies Actions;
```

Ce que nous allons récupérer c'est les données du formulaire : nous utiliserons un objet `Request` sur lequel nous appellerons la méthode asynchrone `formData()`. Puis nous extraierons les valeurs des champs du formulaire avec la méthode `get()` :

```ts
export const actions = {
    login: async () => {
        console.log("Login");

    },
    register: async ({ request }) => {
        const req = await request.formData()
        const username = req.get("username");
        const password = req.get("password");

        console.log(`Register : ${username}, ${password}`)
    }
} satisfies Actions;
```

Vérifions si les valeurs contenues dans `username` et `password` ne sont pas `undefined`. Si c'est le cas, nous retournerons une erreur :

```ts
import { fail } from '@sveltejs/kit';
import type { Actions } from './$types';

export const actions = {
    login: async () => {
        console.log("Login");

    },
    register: async ({ request }) => {
        const req = await request.formData()
        const username = req.get("username");
        const password = req.get("password");

        if (username && password) console.log(`Register : ${username}, ${password}`);
        else return fail(400, {errorMessage: "Missing username or password"})
        
    }
} satisfies Actions;
```

Nous avons vu dans l'autre tutoriel l'utilisation de `throw error(codeStatusHTTP, strMessage)` mais ici nous utilisons une fonction `fail()`. Cette fonction fonctionne comme `error` mais retourne un objet spécifique aux échecs pour les actions.

Maintenant intégrons notre fonction `createUser` mais nous allons rencontrer un problème. Le type des deux variables `username` et `password` n'est pas celui attendu par notre fonction (à savoir des strings). Modifions le code par `const username = req.get("username")?.toString()` et `const password = req.get("password")?.toString();`

```ts
import { createUser } from '$lib/database';

...

export const actions = {

...

    register: async ({ request }) => {
        const req = await request.formData()
        const username = req.get("username")?.toString();
        const password = req.get("password")?.toString();

        if (username && password) {
            createUser(username, password);
            console.log(`Register : ${username}, ${password}`);
        }
        else return fail(400, { errorMessage: "Missing username or password" })

    }
} satisfies Actions;
```

### 3-Gestion des erreurs dans le formulaire

Retour à notre formulaire. Nous allons gérer les erreurs : s'il y a une erreur nous l'afficherons dans le formulaire. Nous devons exporter un `form` en tant que `ActionData`. Ce mot-clé propre à SK nous permet de récupérer notre message d'erreur. Nous utiliserons tout simplement un texte formaté dans une condition `{#if}...{/if}` :

```html
<script lang="ts">
    import type { ActionData } from './$types';

    export let form: ActionData;
</script>

<div class="container">
    <h1 class="is-size-3 has-text-weight-semibold my-4">Login or Register</h1>
    <form method="post">
        <input type="text" class="input my-2" placeholder="Username" name="username" required />
        <input type="password" class="input my-2" placeholder="Password" name="password" required />
        {#if form?.errorMessage}
            <div class="has-text-danger my-2">{form.errorMessage}</div>
        {/if}
        <button type="submit" class="button mt-4 mr-3" formaction="?/register">Register</button>
        <button type="submit" class="button mt-4 is-primary" formaction="?/login">Login</button>
    </form>
</div>
```

Maintenant testons en ajoutant des données sans intérêts et vérifions avec Beekeeper ou SqliteStudio. Si tout fonctionne, les données auront été enregistrées correctement.

### 4-Hash

Mais nous faisons face à un problème dans notre base : les mots de passe sont stockés en clair. Il existe une bibliothèque [bcrypt](https://www.npmjs.com/package/bcrypt) que nous allons importer avec `npm i bcrypt` et `npm i -D @types/bcrypt`.

Retour dans notre fonction `createUser` et calculons le hash. Pour cela notre fonction sera asynchrone et nous appellerons tout simplement la fonction `hash` qui prend deux valeurs, l'élément à hasher et un entier servant de "grain de sel" que l'on peut voir comme un élément augmentant la complexité du hashage (pour aller plus loin : cf [doc](https://www.npmjs.com/package/bcrypt)).

```ts
export const createUser = async (username: string, password: string) => {
    const sql = `INSERT INTO users (username, password, roles)
values ($username, $password, 'admin:moderator')`;
    const hashedPWD = await hash(password, 12)

    db.prepare(sql).run({ username, password: hashedPWD });
}
```

Nous pouvons tester le code directement. Si on veut aller peu plus loin, nous pouvons modifier dans `+page.server.ts` l'appel de `createUser` en le précédent d'un `await` ! Mais notre code fonctionne sans présence.

## 3-Gestion des logins

Nous avons jusqu'ici géré l'enregistrement d'un nouvel utilisateur avec l'utilisation d'une action nommée `register`. Maintenant nous allons gérer un utilisateur déjà enregistré.

### 1-Appel vers la base de données

Dans `database.ts`, nous allons récupérer le nom d'utilisateur et son mot de passe. Puis nous allons récupérer le mot de passe en vérifiant si l'utilisateur existe. Si l'utilisateur existe alors on va comparer le mot de passe stocké avec le mot de passe envoyé. Pour cela, nous allons à nouveau utiliser la bibiothèque `bcrypt` qui dispose d'une fonction `compare` prenant deux paramètres :

- la donnée qui doit être chiffrée (ici notre argument)
- la donnée chiffrée (ici le mot de passe récupéré depuis la base de données)

**Attention**, la fonction `compare` est asynchrone !

Cette fonction retournera un booléen. Maintenant si l'utilisateur n'existe pas alors nous retournerons tout simplement `false`.

Notre code devrait ressembler à  :

```ts
export const checkUserCredential = async (username: string, password: string) => {
    const sql = `SELECT password FROM users WHERE username = $username`;
    const rec = db.prepare(sql).get({ username });
    if (rec) {
        return await compare(password, rec.password);
    }
    return false;
}
```

### 2-Gestion de l'envoi

Maintenant que nous pouvons traiter la vérification, il nous faut gérer la communication entre notre fonction de vérification et notre interface utilisateur. Donc gérer l'action sur le clic du bouton "Login". Nous avions déjà écrit le code pour l'action nommée `register`. Maintenant c'est l'action `login` qui va gérer les accès. Donc nous allons récupérer la requête graĉe à l'objet passé en argument `request`.

Première étape : récupérer les données du formulaire

- utiliser la méthode `formData()` depuis l'objet `request` (attention code asynchrone)
- extraire le mot de passe et le nom d'utilisateur

Seconde étape : vérifier le mot de passe et l'utilisateur

- si le mot de passe et le nom d'utilisateur ne sont pas `undefined` alors nous appellerons la fonction `checkUserCredential` (attention encore au code asynchrone)
- puis nous vérifierons si le retour de la fonction est `false` :

```ts
export const actions = {
    login: async ({ request }) => {
        const req = await request.formData()
        const username = req.get("username")?.toString();
        const password = req.get("password")?.toString();

        if (username && password) {
            const rslt = await checkUserCredential(username, password)
            if (!rslt) {
                return fail(400, { errorMessage: "Incorrect username or password" })
            }
        } else {
            return fail(400, { errorMessage: "Missing username or password" })
        }
    },

    ...

} satisfies Actions;
```

### 3-Sécurité

Notre code de vérification `checkUserCredential` peut poser un problème. Une analyse du temps de réaction peut déterminer si on a un bon nom d'utilisateur mais un mauvais mot de passe. Dans cette fonction si la requête SQL retourne un résultat alors on a un temps de traitement supplémentaire avec `compare()`. Si la requête ne retourne rien, alors la fonction retourne immédiatement `false`. Donc une solution est d'ajouter un temps de latence supplémentaire avec un calcul inutile avec `hash`

```ts
export const checkUserCredential = async (username: string, password: string) => {
    ...
    //  on ajoute un traitement juste avant le dernier return
    await hash(password, 12)
    return false;
}
```

## 4-Sessions avec les cookies

C'est la partie la plus importante et la plus lourde à gérer.

### 1-Bases avec les cookies

Notre vérification fonctionne maintenant nous devons passer au login à proprement parlé. Dans `+page.server.ts`, nous allons gérer les cookies et les sessions dans une fonction helper. Cette fonction aura pour but de récupérer un ID de session et de stocker cet ID dans un cookie.

Dans `+page.server.ts`, nous utilisons des actions nommées et elles peuvent nous donner accès non seulement aux requêtes et à leurs données (notamment les données de formulaire) mais aussi à d'autres choses telles que les cookies. Il existe un type `Cookies` qui dispose de fonctions de gestion, de manipulation. La création d'un cookie sera, comme nous l'avons dit, déporté dans une fonction helper.

Pour créer un cookie, on utilisera la méthode `set()` prenant trois arguments dont deux obligatoires :

- un nom de clé (string)
- une valeur (string)
- des options sous la forme d'un objet (optionnel). Nous y ajouterons un âge maximal

Créons cette fonction helper. Celle-ci aura deux paramètres :

- un type `Cookie` : il sera retourné par l'action nommée `login`
- le nom de l'utilisateur

Cette fonction sera à intégrer [plus tard (sous chapitre 5)](#5-création-du-cookie) dans l'action nommée `login`. Le code ci-dessous n'est qu'un squelette pour indiquer que c'est ici que l'on va créer le cookie :

```ts
const performLogin = (cookie: Cookies, username: string) => {
    cookie.set("sid", "helloworld", {
        maxAge: 3 * 60
    });
}
```

### 2-Gestion des sessions

Pour créer une session, nous allons recourir à un store côté serveur servant de cache pour les sessions valides. Ce store n'est pas celui de Svelte/SK : il sera un peu comme un Redis mais codé localement avec TS. En général, la gestion des sessions s'effectue avec l'utilisation d'une base de données comme Redis. Mais ici nous voulons rester le plus simple possible : donc nous allons gérer en mémoire avec TS les sessions.

Il ne faut pas que nos données soient publiques donc créons un répertoire avec le fichier suivant `./src/lib/sessionStore/index.ts`.

Cette partie est la plus complexe à gérer.

Nous allons gérer un ID de session et pour cela il n'y a pas de bibliothèque à récupérer depuis Internet. Nous utiliserons le package NodeJS `crypto`. Créons une fonction simple qui retourne un chaîne à l'aide de la fonction randomBytes() qui génère un élément aléatoire :

```ts
import { randomBytes } from 'node:crypto';

const getSId = (): string => {
    return randomBytes(32).toString("hex");
}
```

Pour stocker nos ID de session, nous allons créer une map simple qiu va stocker un type que nous allons créer. Ce type contiendra les propriétés suivantes :

- le nom d'utilisateur qui sera une chaîne
- les rôles (un tableau de strings)
- le temps d'invalidation

```ts
type SessionInfo = {
    username: string,
    roles: string[],
    invalidAt: number,
};
```

Et créons un type alias string : `type SID string;` qui nous permettra de savoir quel type manipulons-nous.

Maintenant notre map sera de type `Map<SID, SessionInfo>` : `const sessionStore = new Map<SID, SessionInfo>();`

Donc nos squelettes ressemblent à :

```ts
const getSID = (): SID => {
    return randomBytes(32).toString("hex");
}

const sessionStore = new Map<SID, SessionInfo>();

export const createSession = (username: string, maxAge: number): string => { }
```

### 3-Création d'une session

Abordons l'implémentation d'une création de session. Pour générer un ID de session, nous devons éviter tout doublon dans notre base. Donc une très simple boucle `do...while` fera l'affaire :

```ts
export const createSession = (username: string, maxAge: number): string => {
    let sid: SID;

    do {
        sid = getSID();
    } while (sessionStore.has(sid));
}
```

Gérons les rôles de l'utilisateur. Pour cela nous allons devois créer une fonction dans `+database.ts` que nous verrons juste après. Cette fonction récupère les rôles stockés dans la base et les retournera sous forme d'un tableau de strings. Ces rôles seront stockés dans un type `SessionInfo` auquel on passera le nom d'utilisateur et la date de fin de validité du cookie.

```ts
export const createSession = (username: string, maxAge: number): string => {
    
    ...

    // récupération des rôles
    const roles = getUserRoles(username);

    const sInfo: SessionInfo = {
        username,
        roles,
        invalidAt: maxAge + (Date.now() * 1000), // ici le temps est en milliseconde
    };
}
```

---

Cette fonction `getUserRoles()` sera à créer dans `database.ts`. Cette fonction est très simple :

- créer une requête SQL qui récupère les roles selon le nom d'un utilisateur
- récupérer le résultat avec `get`
- décomposer la chaîne avec `split`
- retourner le résultat

Ce qui donne la fonction :

```ts
export const getUserRoles = (username: string): string[] => {

    const sql = `SELECT roles FROM users WHERE username = $username`;
    const rslt = db.prepare(sql).get({ username });
    if (rslt) {
        return rslt.roles.split(":");
    }
    return [];
}
```

---

Retournons à notre fonction `createSession`, et terminons par la sauvegarde de l'objet SessionInfo dans notre map :

```ts
export const createSession = (username: string, maxAge: number): string => {

    ...

    // on enregistre la session dans notre map
    sessionStore.set(sid, sInfo);

    return sid;
}
```

### 4-Fonction de nettoyage de la base

Il est pertinent de savoir si une session invalide existe dans la base et de la nettoyer si besoin. Nous vérifierons toutes les heures.

Pour cela, il nous faudra créer un marqueur temporel indiquant une projection d'une heure dans le futur :

```ts
let nextClean = Date.now() + 1000 * 60 * 60; // calcul : à partir de ce moment + 1 heure
```

On va recourir à ce marqueur à la fois dans la fonction de nettoyage et dans la création de la session !

Cette fonction va faire le tour de la map et vérifier si une date d'invalidité est dépassée. Donc techniquement on compare la date en cours avec la date d'invalidité. Si cette dernière est plus petite que la date actuelle, alors on supprime la session.

```ts
const clean = () => {
    const now = Date.now();
    for (const [sid, session] of sessionStore) {
        // si la session a dépassé sa date de validité alors on supprime la clé et sa valeur 
        // dans notre sessionStore
        if (session.invalidAt < now) {
            sessionStore.delete(sid);
        }
    }
    nextClean = Date.now() + 1000 * 60 * 60; // on réajuste : une heure de session supplémentaire
}
```

Cette fonction sera utile pour supprimer toute session invalide grâce à la valeur stockée dans `invalidAt`. Nous réajustons à nouveau la valeur de `nextClean`. Cette variable nous sera utile dans `createSession`.

La dernière chose que nous avons faite est le stockage de la session dans notre map : `sessionStore.set(sid, sInfo)`. Maintenant nous pouvons nettoyer si besoin est. Si cela fait plus d'une heure que nous nous sommes connectés et que nous avons conservé une session, il vaut mieux la supprimer de notre map. Mais nous n'allons pas l'utiliser directement mais avec un setTimeout() configuré sur 5 secondes :

```ts
export const createSession = (username: string, maxAge: number): string => {
    ...

    // sauvegarde dans la map
    sessionStore.set(sid, sInfo);

    // on nettoie le sessionStore si besoin
    // indépendamment du retour
    if (Date.now() > nextClean) {
        setTimeout(() => { clean() }, 5000);
    }
    return sid;
}
```

Donc nous allons pouvoir récupérer l'ID de session et le placer dans le cookie.

### 5-Création du cookie

De retour dans `+page.server.ts`, nous allons modifier la fonction `performLogin`. Trois étapes à réaliser :

- créer une constant maxAge à 3 minutes
- récupérer l'ID de session via l'appel de `createSession`
- "assembler" dans le cookie via l'appel de `set`

```ts
const performLogin = (cookie: Cookies, username: string) => {
    const maxAge = 3 * 60;
    const sid = createSession(username, maxAge)
    cookie.set("sid", sid, {
        maxAge
    });
}
```

Ajoutons cette fonction `performLogin`, dans l'action nommée `login`. Notre fonction récupère un type `Cookies` et le nom d'utilisateur. Donc où placer cet appel ? Très simple ! Nous avons vérifier si l'utilisateur avait une autorisation (s'il existait dans notre table SQLite `users`). Nous avions fait une vérification si l'utilisateur n'était pas connu : on retournait un `fail()`. Nous allons placer l'appel de `performLogin` juste après :

```ts
export const actions = {
    login: async ({ request, cookies }) => {
        
        ...

        if (username && password) {
            const rslt = await checkUserCredential(username, password)
            if (!rslt) {
                return fail(400, { errorMessage: "Incorrect username or password" })
            }
            // Création du cookie
            performLogin(cookies, username);
        } else {
            return fail(400, { errorMessage: "Missing username or password" })
        }
    },
    
    ...

} satisfies Actions;
```

## 5-Récupération du cookie avec les hooks

### 1-Définition et création

Documentation sur les [hooks](https://kit.svelte.dev/docs/hooks)

SK nous permet de gérer des événements spécifiques sur l'ensemble de l'application. Ce sont les hooks : il y en a deux types :

- côté client : `hooks.client.ts`
- côté serveur : `hooks.server.ts`

Côté serveur, le hook ou crochet va récupérer toutes les requêtes, et ce, quelque soit l'activité de l'application. Et ce hook va déterminer une réponse. Un hook est fonctionnel dès le démarrage de l'application.

On créera un fichier côté serveur à la racine du code source : `./src/hooks.server.ts`

**ATTENTION** il y a deux choses très importantes à vérifier avec ce fichier :

- le nom du fichier c'est hookS et non hook
- le fichier est à placer à la racine du code source

Le hook va recevoir l'événement représentant la requête et une fonction nommée `resolve` qui va effectuer le rendu de la route et générer une réponse de type `Response`. Nous allons pouvoir modifier les entêtes (headers) et le body de la réponse. Par exemple, on pourra contourner SK pour implémenter des routes de façon programmatique.

La signature de tout hook doit ressembler à :

```ts
import type { Handle } from '@sveltejs/kit';
 
export const handle = (async ({ event, resolve }) => {
  
}) satisfies Handle;
```

De l'objet `event` nous allons extraire la propriété `cookie` : `const cookie  = event.cookie;` Puis de ce cookie, nous allons pouvoir récupérer la valeur depuis la clé `sid` :

```ts
import type { Handle } from '@sveltejs/kit';

export const handle = (async ({ event, resolve }) => {
    const cookie = event.cookies;
    const sid = cookie.get("sid");
    if (sid) {
        
    }
}) satisfies Handle;
```

### 2-Gestion du store de la session

Ce que nous voulons réaliser, c'est récupérer les informations liées à la session. Donc retournons vers `./src/lib/sessionStore/index.ts`. Nous avons notre "store" et nous allons créer une très simple fonction qui prend en argument l'ID de session (dont de type `SID` - rappel : type alias pour `string`) et retournera un type `SessionInfo` ou `undefined` (s'il ne trouve rien):

```ts
export const getSession = (sid: SID): SessionInfo|undefined => {
    const sinfo = sessionStore.get(sid);
}
```

Si sinfo détient une valeur, il nous faut gérer cette session : autrement dit, vérifier si la session est temporellement valide (a-t-on dépassé la date de validité ?). Si la session n'est plus valide, il faut supprimer et retourner un `undefined` :

```ts
export const getSession = (sid: SID): SessionInfo | undefined => {
    const sinfo = sessionStore.get(sid);

    if (sinfo) {
        if (Date.now() > sinfo.invalidAt) {
            sessionStore.delete(sid);
            return undefined;
        }
        return sinfo;
    }
    return undefined;
}
```

### 3-Récupération de la session

Du côté de `hooks.server.ts`, nous allons pouvoir récupérer via getSession() notre `session`. Attention, comme nous avons une possiblité de `undefined` en retour, nous devons gérer ce cas. Que souhaitons-nous faire ? nous souhaitons stocker dans `event.locals`, la session récupérée mais en décomposant le type SessionInfo en ses propriétés. Chacune de ses propriétés sera stockées dans `event.locals`.

L'intérêt du stockage dans event.locals, c'est que n'importe où dans l'application SK, nous allons pouvoir récupérer les données. Donc, *in fine*, nous n'avons **pas besoin de faire X appel de getSession() pour chaque page nécessitant les sessions** . Ces propriétés sont ajoutées dorénavant à chacune des requêtes du serveur.

```ts
import { getSession } from '$lib/sessionStore';
import type { Handle } from '@sveltejs/kit';

export const handle = (async ({ event, resolve }) => {
    const cookie = event.cookies;
    const sid = cookie.get("sid");
    if (sid) {
        const session = getSession(sid);
        if (session) {
            event.locals.username = session.username;
            event.locals.roles = session.roles;
        } else {
            // la session n'est pas trouvée dans le store alors on supprime le cookie
            cookie.delete("sid");
        }
    }
}) satisfies Handle;
```

---

**NB** : TS n'aime pas ce code car username et roles ne sont pas reconnus. Pour corriger cela éditons le fichier `./src/app.d.ts`. C'est dans ce fichier que nous allons pouvoir ajouter nos propriétés. Nous avons par défaut `interface Locals{ }` de commenter. Décommentons-le et ajoutons nos propriétés `username` et `roles`, en prenant en compte qu'ils peuvent être null :

```ts
declare global {
    namespace App {
        // interface Error {}
        interface Locals {
            username?: string,
            roles?: string[],
        }
        // interface PageData {}
        // interface Platform {}
    }
}

export { };
```

[Doc SK sur les types dans app.d.ts](https://kit.svelte.dev/docs/types#app)

---

Dernier problème à gérer le retour ! Que devons-nous retourner ? nous savons que c'est un type `Handle` mais nous ne sommes pas plus avancé. Comme nous l'avons dit, ce hook est un crochet dans la gestion des requêtes (côté serveur ici) donc nous avons une fonction `resolve` asynchrone qui va nous servir pour incorporer notre nouvel `event`, et ce, de façon très simple : il suffit d'appeler `resolve`, de lui passer notre event... et c'est tout :

```ts
export const handle = (async ({ event, resolve }) => {
    const cookie = event.cookies;
    const sid = cookie.get("sid");
    if (sid) {
        const session = getSession(sid);
        if (session) {
            event.locals.username = session.username;
            event.locals.roles = session.roles;
        }
    }
    return await resolve(event);
}) satisfies Handle;
```

**IMPORTANT** : ce hook sera appelé à chaque requête. Il va vérifier si un cookie avec ID de session valide existe.

## 6-Modification de l'interface

Nous allons modifier notre interface et plus précisément le header du layout : une fois le login réussi, nous ne souhaitons plus voir afficher dans le HEADER, le bouton Login mais un bouton Log out. Nous ne pourrons pas faire cette modification directement dans `./src/routes/+layout.svelte` mais dans du côté serveur. SK nous propose une page spéciale nommée [`+layout.server.ts`](https://kit.svelte.dev/docs/routing#layout-layout-server-js) qui a pour but de charger des données : elle propose une seule fonction [`load`](https://kit.svelte.dev/docs/load#layout-data) dont le squelette ressemble à :

```ts
import type { LayoutServerLoad } from "./$types";

export const load = (async () => {

}) satisfies LayoutServerLoad;
```

Nous allons pouvoir récupérer les éléments de l'objet `locals`. Et nous savons que cet objet contient deux données dont le nom que nous allons extraire et retourner comme objet JS :

```ts
import type { LayoutServerLoad } from "./$types";

export const load = (async ({ locals }) => {
    const username = locals.username;
    return { username }
}) satisfies LayoutServerLoad;
```

Maintenant nous allons pouvoir bien récupérer cet objet dans le fichier `./src/routes/+layout.svelte` via le mot-clé `data` :

```html
<script lang="ts">
    import type { LayoutData } from './$types';

    export let data: LayoutData;
</script>
```

Puis nous allons en fonction de la présence de données dans `data`, nous afficherons le bouton Login ou Logout :

```html
<header>
    <nav class="navbar is-primary px-4" aria-label="main navigation">
        ...

        <div class="navbar-end">
            <div class="navbar-item">
                <div class="buttons">
                    {#if data.username}
                        <a href="/logout" class="button is-primary">Log out {data.username}</a>
                    {:else}
                        <a href="/login" class="button is-primary">Log in</a>
                    {/if}
                </div>
            </div>
        </div>
    </nav>
</header>
```

## 7-Problèmes

### 1-Bouton de logout

Si on se connecte avec Login, nous devrions voir en lieu et place du bouton `Log in` un `Log out` mais nous non : le bouton n'est pas modifié quand nous sommes  loggués ! Si on fait un rafraîchissement de la page tout rentre dans l'ordre : le bouton devient un `Log out ...`.

Ce que l'on peut faire, c'est une redirection dans l'action nommée `login` de notre `+layout.server.ts` de `./login`. Pour cela nous allons effectuer un appel de la fonction fournie par SK : `redirect`. Cette fonction sera appelée à l'aide de `throw` :

```ts
export const actions = {
    login: async ({ request, cookies }) => {
        ...

        if (username && password) {
            ...
            throw redirect(303, "/");
        } else {
            return fail(400, { errorMessage: "Missing username or password" })
        }
    },

    ...

} satisfies Actions;
```

Nous effectuons une redirection juste après avoir appelé `performLogin`. Maintenant dès que nous serons bien loggué nous pourrons être redirigé avec le bon bouton de logout.

### 2-Problèmes avec Register

Avec l'action nommée register, il serait bien qu'une fois l'enregistrement réalisée :

- on appelle performLogin() : il est stupide de demander à notre utilisateur de se logguer.
- comme pour le login, il faut que l'on fasse une redirection.

Direction l'action nommée `register` dans `+page.server.ts`. Dans la signature de notre fonction, nous appelons le paramètre `cookies` :

```ts
register: async ({ request, cookies }) => { ... }
```

Puis nous ajoutons les appels de `performLogin()` et de `throw redirect()` :

```ts
export const actions = {
    login: async ({ request, cookies }) => {
        ...
    },
    register: async ({ request, cookies }) => {
        ...

        if (username && password) {
            ...

            performLogin(cookies, username);
            throw redirect(303, "/")
        }
        else return fail(400, { errorMessage: "Missing username or password" })
    }
} satisfies Actions;
```

Maintenant nous pouvons gérer le logout.

## 8-Logout

Créons une nouvelle route `./src/routes/logout` et créons `+page.server.ts`. Pourquoi ne pas créer alors une page svelte ? c'est un choix parmi d'autres. Sans d'un point de vue accessibilité, il conviendrait mieux d'informer l'utilisateur de sa déconnexion réussie.

Notre `+page.server.ts` ne contiendra qu'une seule fonction `load` (comme défini dans la [documentation](https://kit.svelte.dev/docs/routing#page-page-server-js)). Nous allons utiliser le cookie afin de récupérer l'ID de session :

```ts
import type { PageServerLoad } from "../$types";

export const load = (async ({ cookies }) => {
    const sid = cookies.get("sid");

}) satisfies PageServerLoad;
```

Puis nous allons vérifier si l'ID existe (donc que le cookie existe), et nous le supprimerons tout simplement avec `cookies.delete("sid")`. Puis nous devons nettoyer notre `sessionStore` en créant une fonction `deleteSession()` dans `./src/lib/sessionStore/index.ts`. Cette fonction récupèrera l'ID et s'il existe le supprimer de la map :

```ts
// ./src/lib/sessionStore/index.ts

export const deleteSession = (sid: SID): void => {
    sessionStore.delete(sid)
};
```

**Remarque** : si nous avions créé une page informant l'utilisateur de sa déconnexion, alors on aurait tout intérêt pour récupérer le retour de la fonction `delete()` qui est un booléen.

Maintenant nous pouvons appeler notre nouvelle fonction dans `./src/routes/logout/+page.server.ts` :

```ts
import { deleteSession } from "$lib/sessionStore";
import type { PageServerLoad } from "../$types";

export const load = (async ({ cookies }) => {
    const sid = cookies.get("sid");
    if (sid) {
        cookies.delete("sid");
        deleteSession(sid);
    }
}) satisfies PageServerLoad;
```

Enfin il faudra faire un appel de redirection à l'aide de `throw` et `redirect`

```ts
export const load = (async ({ cookies }) => {
    
    ...

    throw redirect(303, "/");
}) satisfies PageServerLoad;
```

### Bug : préchargement des données

Mais si nous nous déconnectons depuis "/" nous aurons un nouveau bug d'affichage : nous n'aurons pas le bouton `Login` mais toujours le message Logout. Et si on rafraîchit la page : nous obtenons le bouton `Login`.

Ici il faut demander à SK de NE PAS précharger les données. Lorsque l'on clique sur un lien SK fait un préchargement des données or ce comportement est dérangeant car nos données ont été modifiées ([doc](https://kit.svelte.dev/docs/link-options#data-sveltekit-preload-data))

Donc pour empêcher ce comportement nous allons inhiber le préchargement au niveau de la balise HTML `<a href="logout">` en ajoutant l'attribut `data-svelte-kit-preload-data` associé à la valeur `off` :

```html
<-- ./src/routes/+layout.svelte -->
<a href="/logout" class="button is-primary" data-sveltekit-preload-data="off">
    Log out {data.username}
</a>
```

Mais si l'on teste, ce ne sera pas encore efficace. Il faut rajouter un autre attribut qui indique que c'est le navigateur qui doit recharder les données et non SK : `data-sveltekit-reload` ([doc](https://kit.svelte.dev/docs/link-options#data-sveltekit-reload)) :

```html
<a
    href="/logout"
    class="button is-primary"
    data-sveltekit-preload-data="off"
    data-sveltekit-reload
>
    Log out {data.username}
</a>
```

Si nous testons, nous observons que tout est effectivement rentré dans l'ordre.

## 9-Gestion des autorisations

Maintenant que nous avons gérer la plus grosse partie du programme et la plus complexe concernant le login, nous allons pouvoir faire en sorte que les modifications de la base de données ne peuvent être appliquées qu'aux seules personnes habilitées.

Lorsqu'on sélectionne un album, toute personne peut modifier l'album donc comme nous avons défini des rôles nous pouvons maintenant gérer cette problématique.

Pour étudier les roles, créons un utilisateur d'abord avec notre page Login/register avec un nom `notAdmin` et un mot de passe "123" par exemple. Et modifions avec SQLite, le role en supprimant `admin` donc en ne laissant que `moderator`

Donc nous ne souhaitons autoriser sur la page `./src/routes/album/[id]/+page.svelte` l'accès au champ de modification uniquement aux admins. C'est du côté `./src/routes/album/[id]/+page.server.ts` que l'on doit apporter une modification. On pourrait cacher le formulaire. Ici nous allons tout simplement empêcher l'envoi de données si l'utilisateur n'est pas un admin.

Nous avons notre hook qui va nous faciliter la vie.

Donc nous allons vérifier dans l'action nommée `updateAlbumTitle` :

- si l'utilisateur dans `locals` n'existe pas
- **OU** si la valeur "admin" dans `locals.roles` n'est pas trouvée

Si aucune des deux conditions n'est validée alors on retournera une erreur à l'aide de `throw` et `error()` :

```ts
export const actions = {
    updateAlbumTitle: async ({ request, locals }) => {

        if (!locals.username || !locals.roles?.includes("admin")) {
            throw error(401,
                {
                    message: "Unauthorized"
                });
        }

        ...
    }
}
```

Nous pouvons tester maintenant :

- directement sans connexion
- en nous logguant avec `notAdmin`

Normalement, nous ne pouvons pas modifier le titre de l'album !

Apportons une légère modification : cachons le formulaire !

À nouveau, c'est du côté de `+page.server.ts` que nous devons regarder. Et plus précisément du côté de la fonction `load()`, nous allons rajouter tout simplement un nouveau champ contenant une simple valeur booléenne qui sera récupérée du côté `+page.svelte`.

```ts
export const load = (({ params }) => {
    ...

    return {
        id,
        album,
        tracks,
        authorization: false,
    };

}) satisfies PageServerLoad;
```

Maintenant modifions notre `+page.svelte`. Nous devons modifier le type de données reçues :

```html
<script lang="ts">
...

    // conversion
    const d = data as {
        id: number;
        album: Album;
        tracks: AlbumTracks[];
        authorization: boolean; // nouveau type
    };
</script>

...

<div class="m-3">
    ...

    {#if d.authorization}
        <h2 class="is-size-3 mb-4 mt-6">Update du titre de l'album</h2>

        <form method="post" action="?/updateAlbumTitle">
            <input
                class="input"
                type="text"
                name="albumTitle"
                placeholder={d.album.albumTitle}
                style="max-width: 50ch;"
            />
            <input type="hidden" name="albumId" value={d.album.albumId} />
            <button class="button is-primary" type="submit">Update</button>
        </form>
    {/if}
</div>
```

Nous n'avons plus à toucher à `+page.svelte`. Nous pouvons nous concentrer sur `+page.server.ts` et rendons le champ `authorization` "dynamique" en récupérant les valeurs de `locals` :

```ts
export const load = (({ params, locals }) => {
    const id = +params.id;
    const authorization = ((locals.username) && (locals.roles?.includes("admin")));
    
    ...

    return {
        id,
        album,
        tracks,
        authorization,
    };

}) satisfies PageServerLoad;
```

Maintenant nous pouvons tester les accès en mode anonyme, loggué avec "notAdmin" (dans ces deux cas, pas d'affichage du formulaire) et loggué avec un utilisateur ayant les droits de modification (dans ce cas, le formulaire sera bien affiché).
